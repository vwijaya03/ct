<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
	
	<br>
	
	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addProductModal">Add Product</button>

	<table class="table product_table">
	    <thead>
	      <tr>
	        <th>Product Name</th>
	        <th>Quantity</th>
	        <th>Price</th>
	        <th>Total</th>
	      </tr>
	    </thead>
	    <tbody>
	      
	    </tbody>
  	</table>

  	<table class="table">
  		<thead>
	      <tr>
	        <th></th>
	        <th></th>
	        <th></th>
	        <th></th>
	      </tr>
	    </thead>
	    <tbody>
	      <tr>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td></td>
	      	<td class="sum_total"></td>
	      </tr>
	    </tbody>
  	</table>

	<!-- Modal -->
	<div id="addProductModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add New Product</h4>
				</div>

				<div class="modal-body">
					<label>Product Name</label>
					<input type="text" class="form-control" id="name" />

					<label>Quantity</label>
					<input type="text" class="form-control" id="quantity" />

					<label>Price</label>
					<input type="text" class="form-control" id="price" />
					
					<br>

					<button class="btn btn-info" onclick="submitProduct()">Submit</button>
				</div>
			</div>

		</div>
	</div>

	<!-- Modal -->
	<div id="editProductModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Product</h4>
				</div>

				<div class="modal-body">
					<label>Product Name</label>
					<input type="text" class="form-control" id="edit_name" />

					<label>Quantity</label>
					<input type="text" class="form-control" id="edit_quantity" />

					<label>Price</label>
					<input type="text" class="form-control" id="edit_price" />
					
					<input type="hidden" id="product_id" />

					<br>

					<button class="btn btn-info" onclick="updateProduct()">Update</button>
				</div>
			</div>

		</div>
	</div>

</body>

<script type="text/javascript">
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});

	var list_products = $.ajax({
        type: "GET",
        url: 'all-product',
        dataType: "json",
        cache : false,
        success: function(result){
        	var sum = 0;
      		result.products.forEach(function(val) {
      			console.log(val);
      			
      			$('.product_table tbody').prepend('<tr><td>'+val.name+'</td><td>'+val.quantity+'</td><td>'+val.price+'</td><td>'+val.quantity * val.price+'</td><td><button class="btn btn-default" onclick="editProduct('+val.id+')">Edit</button></td></tr>');
      			sum += (Number(val.price) * Number(val.quantity));

      			$('.sum_total').html(sum);
      		});
        } ,error: function(xhr, status, error) {
          alert(error);
        },

    });

    function reload_data()
    {
    	$('.product_table tbody tr').remove();
    	var sum = 0;
    	$.ajax({
	        type: "GET",
	        url: 'all-product',
	        dataType: "json",
	        cache : false,
	        success: function(result){
	      		result.products.forEach(function(val) {
	      			console.log(val);
	      			$('.product_table tbody').prepend('<tr><td>'+val.name+'</td><td>'+val.quantity+'</td><td>'+val.price+'</td><td>'+val.quantity * val.price+'</td><td><button class="btn btn-default" onclick="editProduct('+val.id+')">Edit</button></td></tr>');

	      			sum += (Number(val.price) * Number(val.quantity));

      				$('.sum_total').html(sum);
	      		});
	        } ,error: function(xhr, status, error) {
	          alert(error);
	        },

	    });
    }

    function editProduct(id)
    {
    	var full_url = '{{ url("/") }}';

    	var list_products = $.ajax({
	        type: "GET",
	        url: full_url+'/edit-product/'+id,
	        dataType: "json",
	        cache : false,
	        success: function(result){

	        	$('#edit_name').val(result.product.name);
	        	$('#edit_quantity').val(result.product.quantity);
	        	$('#edit_price').val(result.product.price);
	        	$('#product_id').val(result.product.id);

	      		$('#editProductModal').modal('show');
	        } ,error: function(xhr, status, error) {
	          alert(error);
	        },

	    });
    }

    function updateProduct()
    {
    	var full_url = '{{ url("/") }}';
    	var product_id = $('#product_id').val();

    	var args = {};
    	args.name = $('#edit_name').val();
    	args.quantity = $('#edit_quantity').val();
    	args.price = $('#edit_price').val();
    	args.total = args.quantity * args.price;

    	console.log(args);

    	$.ajax({
	        type: "POST",
	        url: full_url+'/edit-product/'+product_id,
	        data: args,
	        dataType: "json",
	        cache : false,
	        success: function(result){
	        	//$('.product_table tbody').remove();
	      		reload_data();
	      		$('#editProductModal').modal('hide');
	        } ,error: function(xhr, status, error) {
	          alert(error);
	        },

	    });
    }

    function submitProduct()
    {
    	var args = {};
    	args.name = $('#name').val();
    	args.quantity = $('#quantity').val();
    	args.price = $('#price').val();
    	args.total = args.quantity * args.price;

    	console.log(args);

    	$.ajax({
	        type: "POST",
	        url: 'add-product',
	        data: args,
	        dataType: "json",
	        cache : false,
	        success: function(result){
	        	//$('.product_table tbody').remove();
	      		reload_data();
	        } ,error: function(xhr, status, error) {
	          alert(error);
	        },

	    });
    }
</script>
</html>