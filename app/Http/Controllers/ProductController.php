<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductModel;

class ProductController extends Controller
{
    public function __construct(ProductModel $productModel)
    {
        $this->productModel = $productModel;
    }

    public function getProduct()
    {
        return view('product/index');
    }

    public function getAllProduct()
    {
    	$products = $this->productModel->getProduct();

        return response()->json([
		    'products' => $products
		]);
    }

    public function getAddProduct()
    {
    	return view('product/add', ['user' => Auth::user(), 'page_title' => $this->page_title]);
    }

    public function postAddProduct(Request $request)
    {
    	$requested = [];
    	$requested['name'] = $request->get('name');
    	$requested['quantity'] = $request->get('quantity');
    	$requested['price'] = $request->get('price');
    	$requested['total'] = $request->get('total');

    	$result = $this->productModel->postAddProduct($requested);
    	
    	return response()->json([
		    'code' => 200
		]);
    }

    public function getEditProduct($id)
    {
    	$product = $this->productModel->getOneProduct($id);

    	return response()->json([
		    'product' => $product
		]);
    }

    public function postEditProduct(Request $request, $id)
    {
    	$requested = [];
    	$requested['name'] = $request->get('name');
    	$requested['quantity'] = $request->get('quantity');
    	$requested['price'] = $request->get('price');
    	$requested['total'] = $request->get('total');

    	$result = $this->productModel->postEditProduct($requested, $id);

    	return response()->json([
		    'code' => 200
		]);
    }
}
