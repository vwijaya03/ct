<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductModel extends Model
{
    protected $table = 'products';
	protected $primaryKey = 'id';
    protected $fillable = ['name', 'quantity', 'price', 'total', 'delete'];

    private $success_update_msg = 'Data has been updated';
    private $success_add_msg = 'Data has been added';
    private $success_delete_msg = 'Data has been deleted';

    public function getProduct()
    {
        $product = $this->select('id', 'name', 'quantity', 'price')
        ->where('delete', 0)
        ->get();

        return $product;
    }

    public function getOneProduct($id)
    {
        $product = $this->select('id', 'name', 'quantity', 'price')
        ->where('id', $id)
        ->where('delete', 0)
        ->first();

        return $product;
    }

    public function postAddProduct($param)
    {
        $result = [];

        $final = DB::transaction(function () use($param, $result) {
            $data = $this->create([
                'name' => $param['name'],
                'quantity' => $param['quantity'],
                'price' => $param['price'],
                'total' => $param['total'],
                'delete' => 0
            ]);

            $result[0] = $data->id;
            $result[1] = $this->success_add_msg;

            return $result;
        });

        return $final;
    }

    public function postEditProduct($param, $id)
    {
        DB::transaction(function () use($param, $id) {
            $this->where('id', $id)->where('delete', 0)->update([
                'name' => $param['name'],
                'quantity' => $param['quantity'],
                'price' => $param['price'],
                'total' => $param['total']
            ]);
        });

        return $this->success_update_msg;
    }
}
